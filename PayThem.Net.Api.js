/**
 * PayThem.Net.API
 *
 * NodeJS API wrapper for PayThem.Net API
 *
 * @author Vincent Seaborne vince@paythem.net
 *
 * @copyright Copyright PayThem.Net WLL, 2021
 *
 */
class PayThemNetApi {
    publicKey;
    sender;
    queryObject;
    encryptor;
    callback;
    internalErrorCallback;
    host;

    /**
     * Constructor
     *
     * Creates the main query object
     * Sets the encryption details
     * Sets defaults to success and error responses
     *
     * @param encryptionKey The encryption / private key provided by PayThem.Net
     */
    constructor(encryptionKey) {
        this.queryObject = new query();
        this.sender = new CommandSender();
        this.encryptor = new Encryptor(encryptionKey, 'UTF8', 'base64');
        this.callback = function (data) { };
        this.errorCallback = function (data) { };
        this.host = 'vvsdemo.paythem.net';
    }

    /**
     * Set server details
     *
     * Set the timezone that the current request is coming from
     * Example: 'Africa/Harare'
     * Sets the public key as supplied by PayThem.Net
     *
     * @param timeZone The current client timezone string
     * @param publicKey The public key as supplied by PayThem.Net
     *
     */
    setServer(timeZone, publicKey, IP) {
        this.queryObject.SERVER_TIMEZONE = timeZone;
        this.queryObject.PUBLIC_KEY = publicKey;
        this.queryObject.SOURCE_IP = IP;
        this.publicKey = publicKey;
    }

    /**
     * Set current user
     *
     * Sets the operating user for the API calls
     *
     * @param username The username as supplied by PayThem.Net
     * @param password The user's password as supplied by PayThem.Net
     *
     */
    setUser(username, password) {
        this.queryObject.USERNAME = username;
        this.queryObject.PASSWORD = password;
    }

    /**
     * Success callback
     *
     * Function to call on success of the next run response
     *
     * @param callback The function to call back to
     *
     */
    onSuccess(callback) {
        this.callback = callback;
    }

    /**
     * Error callback
     *
     * Function to call on any errors
     *
     * @param callback The function to call back to
     *
     */
    onInternalError(callback) {
        this.internalErrorCallback = callback;
    }

    /**
     * Get OEM list
     *
     * Retrieves a list of all available OEMs
     *
     */
    get_OEMList() {
        this.queryObject.FUNCTION = "get_OEMList";
        this.callServer();
    }

    /**
     * Get brand list
     *
     * Retrieves a list of all available brands
     *
     */
    get_BrandList() {
        this.queryObject.FUNCTION = "get_BrandList";
        this.callServer();
    }

    /**
     * Get product list
     *
     * Retrieves a list of all available products
     *
     */
    get_ProductList() {
        this.queryObject.FUNCTION = "get_ProductList";
        this.callServer();
    }

    /**
     * Get product availability
     *
     * Retrieves the availability of the specified product
     *
     * @param productId The product ID of the product to check
     *
     */
    get_ProductAvailability(productId) {
        this.queryObject.FUNCTION = "get_ProductAvailability";
        this.queryObject.addParameter('PRODUCT_ID', productId);
        this.callServer();
    }

    /**
     * Get product availability for all
     *
     * Retrieves the availability of all the product
     *
     */
    get_AllProductAvailability() {
        this.queryObject.FUNCTION = "get_AllProductAvailability";
        this.callServer();
    }

    /**
     * Get account balance
     *
     * Retrieves the account balance of the current user
     *
     */
    get_AccountBalance() {
        this.queryObject.FUNCTION = "get_AccountBalance";
        this.callServer();
    }

    /**
     * Get vouchers
     *
     * Purchases the vouchers specified
     *
     * @param productId The product ID of the product to purchase
     * @param quantity The quantity of the product to purchase
     * @param reference The internal reference of the order
     *
     */
    get_Vouchers(productId, quantity, reference='') {
        this.queryObject.FUNCTION = "get_AccountBalance";
        this.queryObject.addParameter('PRODUCT_ID', productId);
        this.queryObject.addParameter('QUANTITY', quantity);
        this.queryObject.addParameter('REFERENCE_ID', reference);
        this.callServer();
    }

    /**
     * Get financial transaction by date range
     *
     * Retrieves all financial transactions for current user
     * This is limited to the from and to date passed
     *
     * @param fromDate The date to start retrieving from
     * @param toDate The date to retrieve to
     */
    get_FinancialTransaction_ByDateRange(fromDate, toDate) {
        this.queryObject.FUNCTION = "get_FinancialTransaction_ByDateRange";
        this.queryObject.addParameter('FROM_DATE', fromDate);
        this.queryObject.addParameter('TO_DATE', toDate);
        this.callServer();
    }

    /**
     * Get sales transactions by date range
     *
     * Retrieves all sales transactions for current user
     * This is limited to the from and to date passed
     *
     * @param fromDate The date to start retrieving from
     * @param toDate The date to retrieve to
     *
     */
    get_SalesTransaction_ByDateRange(fromDate, toDate) {
        this.queryObject.FUNCTION = "get_SalesTransaction_ByDateRange";
        this.queryObject.addParameter('FROM_DATE', fromDate);
        this.queryObject.addParameter('TO_DATE', toDate);
        this.callServer();
    }

    /**
     * Get product output formats
     *
     * Retrieves all output formats for products vouchers
     *
     */
    get_ProductFormats(){
        this.queryObject.FUNCTION = "get_ProductFormats";
        this.callServer();
    }

    /**
     * Get Transaction By Reference ID
     *
     * Retrieves a transaction specified by the reference ID
     *
     * @param referenceId The reference of the transaction to retrieve
     *
     */
    get_TransactionByReferenceId(referenceId){
        this.queryObject.FUNCTION = "get_TransactionByReferenceId";
        this.queryObject.addParameter('REFERENCE_ID', referenceId);
        this.callServer();
    }

    /**
     * Get Product Info
     *
     * Retrieves all information on the specified product
	 * 
	 * @param productId The product ID of the product to retrieve
     *
     */
    get_ProductInfo(productId){
        this.queryObject.FUNCTION = "get_ProductInfo";
        this.queryObject.addParameter('PRODUCT_ID', productId);
        this.callServer();
    }

    /**
     * Get Max Allowed Vouchers Per Call
     *
     * Retrieves the maximum allowed voucher purchase amount per call
     *
     */
    get_MaxAllowedVouchersPerCall(){
        this.queryObject.FUNCTION = "get_MaxAllowedVouchersPerCall";
        this.callServer();
    }

    /**
     * Get last sale
     *
     * Retrieves the last sale information for the current user
     *
     */
    get_LastSale(){
        this.queryObject.FUNCTION = "get_LastSale";
        this.callServer();
    }

    /**
     * Live environment
     *
     * Sets the calling environment to the production URL
     *
     */
    liveEnvironment() {
        this.host = 'vvs.paythem.net';
    }

    /**
     * Call server
     *
     * NOT to be used by the client
     * This internal call sends the required commands to the server
     *
     */
    callServer() {
        var encryptedString = this.encryptor.encryptText(this.queryObject.get());
        this.sender.send(this.host, encryptedString, this.publicKey, this.encryptor.IV, this.encryptor.hMac, this.callback, this.internalErrorCallback);
    }
}

class CommandSender {
    constructor() {

    }

    send(host, content, publicKey, IV, hMac, callback, internalError) {
        var https = require('https');
        var options = {
            'method': 'POST',
            'hostname': host,
            'path': '/API/2824/',
            'headers': {
                'X-Public-Key': publicKey,
                'X-Hash': hMac
            },
            'maxRedirects': 20
        };
        try {
            var req = https.request(options, function (res) {
                var chunks = [];

                res.on("data", function (chunk) {
                    chunks.push(chunk);
                });

                res.on("end", function (chunk) {
                    var body = Buffer.concat(chunks);
                    try {
                        callback(body.toString());
                    } catch (ee) {
                        internalError(ee);
                    }
                });

                res.on("error", function (error) {
                    try {
                        internalError(error);
                    } catch (ee) {
                        console.error(ee)
                    }
                });
            });
            var postData = "------WebKitFormBoundaryPayThem.Net\r\nContent-Disposition: form-data; name=\"PUBLIC_KEY\"\r\n\r\n" + publicKey + "\r\n------WebKitFormBoundaryPayThem.Net\r\nContent-Disposition: form-data; name=\"CONTENT\"\r\n\r\n" + content + "\r\n------WebKitFormBoundaryPayThem.Net\r\nContent-Disposition: form-data; name=\"ZAPI\"\r\n\r\n" + IV + "\r\n------WebKitFormBoundaryPayThem.Net--";
            req.setHeader('content-type', 'multipart/form-data; boundary=----WebKitFormBoundaryPayThem.Net');
            req.write(postData);
            req.end();
        } catch (sE) {
            internalError(sE);
        }
    }
}

function query() {
    var obj = {};
    obj.STUB = "";
    obj.FUNCTION = "";
    obj.SERVER_TIMEZONE = "";
    obj.SERVER_TIMESTAMP = "";
    obj.API_FRAMEWORK = "NODE.JS";
    obj.API_VERSION = currentVersion;
    obj.NODE_VERSION = version;
    obj.PUBLIC_KEY = "";
    obj.PARAMETERS = {};
    obj.SOURCE_IP = '';
    obj.USERNAME = "U04425";
    obj.PASSWORD = "p";
    obj.addParameter = function (key, value) {
        obj.PARAMETERS[key] = value;
    };
    obj.get = function () {
        obj.setTime();
        obj.STUB = Math.floor(1000000000 + Math.random() * 9000000000);
        var stringified = JSON.stringify(this);
        obj.PARAMETERS = {};
        obj.FUNCTION = '';
        return stringified;
    };
    obj.setTime = function () {
        let date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();
        obj.SERVER_TIMESTAMP = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
    };
    return obj;
}

var cc = require("crypto");
const { version } = require('process');
const currentVersion = '2.2.0';

class Encryptor {
    hMac;
    constructor(encryptionKey, encoding, outcoding) {
        this.key = encryptionKey;
        this.encoding = encoding;
        this.outcoding = outcoding;
    }

    encryptText(content) {
        this.IV = Buffer.from(Math.floor(1000000000000000 + Math.random() * 9000000000000000).toString());
        var keyBuff = new Buffer.from(this.key);
        this.hMac = cc.createHmac('sha256', this.key).update(content).digest('HEX');
        var cipher = cc.createCipheriv("aes-256-cbc", keyBuff, this.IV);
        var result = cipher.update(content, this.encoding, this.outcoding);
        var final = cipher.final(this.outcoding);
        result += final;
        return result;
    }
}

module.exports = PayThemNetApi;